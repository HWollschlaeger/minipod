#!/bin/bash

podman pod create --replace=true --name "$POD_NAME" --network="$MINIPOD_PODMAN_NETWORK"

podman create --pod "$POD_NAME" \
    --name "$APP_NAME" \
    --env-file <(envsubst <./config/"$MINIPOD_ENV") \
    --label "io.containers.autoupdate=registry" \
    ghcr.io/mat-o-lab/maptomethod:latest gunicorn -b 0.0.0.0:8080 wsgi:app --workers=3
