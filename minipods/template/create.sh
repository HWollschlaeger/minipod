#!/bin/bash

podman pod create --replace=true --name "$POD_NAME" --network="$MINIPOD_PODMAN_NETWORK"

# Repeat something like this for every container
podman create --pod "$POD_NAME" \
    --name "$APP_NAME"_CONTAINER_NAME \
    --env-file <(envsubst <./config/"$MINIPOD_ENV") \
    --env "VARIABLE_THAT_DOES_NOT_DEPEND_ON_ENV=FOO" \
    --volume "$APP_NAME"_UNIQUE_VOLUMENAME:TARGET \
    YOUR_IMAGE
