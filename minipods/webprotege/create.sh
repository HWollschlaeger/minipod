#!/bin/bash

podman pod create --replace=true --name "$POD_NAME" \
    --network="$MINIPOD_PODMAN_NETWORK"

podman create --pod "$POD_NAME" \
    --name "$APP_NAME"_mongo \
    --volume "$APP_NAME"_mongo:/data/db \
    --volume "$APP_NAME"_mongo_configdb:/data/configdb \
    docker.io/library/mongo:4.1-bionic

podman create --pod "$POD_NAME" \
    --name "$APP_NAME" \
    --requires "$APP_NAME"_mongo \
    --env "webprotege.mongodb.host=127.0.0.1" \
    --volume "$APP_NAME"_webprotege:/srv/webprotege \
    docker.io/protegeproject/webprotege:latest
