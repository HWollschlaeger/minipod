#!/bin/bash

podman pod create --replace=true --name "$POD_NAME" \
    -p="$MINIPOD_PORT_HTTP:$MINIPOD_PORT_HTTP" \
    -p="$MINIPOD_PORT_HTTPS:$MINIPOD_PORT_HTTPS" \
    --network="$MINIPOD_PODMAN_NETWORK"

podman create --pod "$POD_NAME" \
    --name "$APP_NAME" \
    --env-file <(envsubst <./config/"$MINIPOD_ENV") \
    --env "TRAEFIK_PROVIDERS_FILE_DIRECTORY=/etc/traefik/dynamic_conf/" \
    --volume "$MINIPOD_TRAEFIK_DYNAMIC_CONFIG_DIR":/etc/traefik/dynamic_conf/ \
    docker.io/library/traefik
