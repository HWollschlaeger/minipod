#!/bin/bash

log() {
    printf 'CKAN init script: %s\n' "$@"
}

for extension in "$CKAN__EXTENSIONS_DIR"*; do
    if [ -d "$extension" ]; then
        log "Installing extension $extension"
        ckan config-tool "$CKAN_INI" -s DEFAULT "debug = true"
        cd "$extension" || exit
        python3 "$extension"/setup.py develop
        cd /usr/lib/ckan/ || exit
    fi
done

create_admin_user() {
    log "Creating admin user"
    user_query=$(ckan user show "$CKAN_SYSADMIN_NAME")
    if [[ $user_query == "User: None" ]]; then
        ckan user add "$CKAN_SYSADMIN_NAME" \
            password="$CKAN_SYSADMIN_PASSWORD" \
            email="$CKAN_SYSADMIN_EMAIL"
        ckan sysadmin add "$CKAN_SYSADMIN_NAME"
    fi
}

# Only init the container if there is no config and CKAN_SITE_URL is set
if [ ! -f "$CKAN_INI" ] && [ ! -v "$CKAN_SITE_URL" ]; then
    log "Generating config"
    ckan generate config "$CKAN_INI"
    if [ -f "/etc/ckan/merge.ini" ]; then
        log "Merging config"
        ckan config-tool -f /etc/ckan/merge.ini "$CKAN_INI"
    fi
    log "Init database"
    sleep 10
    ckan db init
    create_admin_user
fi

exec "$@"
