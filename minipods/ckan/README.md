## Usage

### Extensions

#### Included extensions

Currently are Included:

- Saml2auth

To use an Included extension:

1. Set `IMAGE_STAGE` in your environment config [file](./config/env-minipod-test#L42) to `extensions`.
1. Add the extension to `ckan.plugins` in your [CKAN config](./config/minipod-test-ckan.ini#L37).

### Create an extension

1. Generate extension:

   ```bash
   podman run --interactive --tty --rm -v $(pwd):/root/extensions ckan-development /bin/bash -c "ckan generate extension --output-dir /root/extensions"
   ```

1. Add your plugin source path to the environment file by setting `PLUGIN_SRC_PATH`.

1. [Enable the plugin](https://docs.ckan.org/en/2.9/extensions/tutorial.html#enabling-the-plugin),
   `ckan.ini` is in this repository.
