#!/bin/bash

# Only add the debug port, when $DEBUG_PORT is defined
args=("--replace=true" "--name=$POD_NAME" "--network=$MINIPOD_PODMAN_NETWORK")
podman pod create "${args[@]}"

podman create --pod "$POD_NAME" \
    --name "$APP_NAME"_postgresql \
    --env-file <(envsubst <./config/"$MINIPOD_ENV") \
    --env "PGDATA=/var/lib/postgresql/data/db" \
    --volume "$APP_NAME"_postgresql_data:/var/lib/postgresql/data \
    postgresql:minipod

podman create --pod "$POD_NAME" \
    --name "$APP_NAME"_solr \
    --volume "$APP_NAME"_solr_data:/opt/solr/server/solr/ckan/data \
    solr:minipod

podman create --pod "$POD_NAME" \
    --name "$APP_NAME"_redis \
    --volume "$APP_NAME"_redis_data:/data \
    docker.io/library/redis:alpine

podman create --pod "$POD_NAME" \
    --name "$APP_NAME" \
    --requires "$APP_NAME"_postgresql \
    --requires "$APP_NAME"_solr \
    --requires "$APP_NAME"_redis \
    --env-file <(envsubst <./config/"$MINIPOD_ENV") \
    --env "CKAN_REDIS_URL=redis://localhost:6379/1" \
    --env "CKAN_SQLALCHEMY_URL=postgresql://ckan:$POSTGRES_PASSWORD@localhost:5432/ckan" \
    --env "CKAN_SOLR_URL=http://localhost:8983/solr/ckan" \
    --volume "$APP_NAME"_ckan_storage:/var/lib/ckan \
    --volume "$(realpath "$CKAN__INI_FILE")":/etc/ckan/merge.ini \
    ckan_"$IMAGE_STAGE":minipod
# --volume "$(realpath "$PLUGIN_SRC_PATH")":/usr/lib/ckanext/"$(basename "$PLUGIN_SRC_PATH")" \
