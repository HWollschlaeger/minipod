## Set up GitLab test runner

As root on remote server:

1. [Install minipod dependencies](README#install-dependencies)
1. [Create environment user](README#create-environment-user) `env-minipod-test`
