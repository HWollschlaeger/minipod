SHELL := /bin/bash
develop: disable-env-minipod-test nuke enable-env-minipod-test

encrypt:
	age -R <(ssh env-minipod-test@metameat.xyz "cat ~/.ssh/id_rsa.pub") env-minipod-test-secrets > env-minipod-test.age
decrypt:
	age -d -i <(ssh env-minipod-test@metameat.xyz "cat ~/.ssh/id_rsa") env-minipod-test.age > env-minipod-test-secrets

nuke: 
	if [ "$(USER)" = env-minipod-test ]; then \
		podman system prune --force --volumes; \
	fi

enable-env-minipod-test:
	./minipod enable env-minipod-test

disable-env-minipod-test:
	./minipod disable env-minipod-test

lint:
	git ls-files | xargs file | grep Bourne | cut -d ':' -f1 | xargs shellcheck -e SC1091

test: nuke enable-env-minipod-test disable-env-minipod-test

.PHONY: enable-env-minipod-test disable-env-minipod-test lint test nuke encrypt decrypt
